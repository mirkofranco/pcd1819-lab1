package lab1.statistics;

import java.io.IOException;
import java.util.HashMap;

public class TestPCD1819Statistics {

    public static void main(String[] args){
        //ATTENZIONE: modificare i percorsi
        MultiMap<Integer, Double> input = new MultiMap<>();
        try {
            PCD1819Statistics.produceSynthetizedBalanceSheet(input, "/Users/mirko/Documents/PCD/pcd1819-lab1/lab1/statistics/esempi-IOFile/input.txt", "/Users/mirko/Documents/PCD/pcd1819-lab1/lab1/statistics/esempi-IOFile/outputPersonal.txt");
        }catch(IOException e){
            System.out.println("Si è verificato un errore");
        }//trycatch
        System.out.println(input);


    }//main
}
