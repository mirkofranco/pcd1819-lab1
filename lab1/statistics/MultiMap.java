package lab1.statistics;

import java.util.*;

/**
 * This class implements a MultiMap data structure. A MultiMap is an associative structure where multiple elements can be mapped
 * under a one key. An an example consider the following sequence of statements:
 * 1. map.put(k1, v1);
 * 2. map.put(k1, v2);
 * 3. List<V> = map.get(k1);
 * 
 * The last operation should return a List<V> of values comprised of the entries {v1, v2}
 */
public class MultiMap<K, V> {

	Map<K, Collection<V>> multimap;

	/**
	 * Sole constructor of the class which builds an empty MultiMap.
	 */
	public MultiMap() {
		multimap = new Hashtable<>();
	}//MultiMap
		
	/**
	 * Stores the value (value)given as an input under the key (key).
	 * 
	 * @param key: 		key parameter which serves as an index in the MultiMap data structure
	 * @param value: 	value parameter to be index under the key
	 */
	public void put(K key, V value) {
        if(multimap.get(key) == null)
            multimap.put(key, new ArrayList<V>());
        multimap.get(key).add(value);
	}//put
	
	/**
	 * Returns a Set view of the keys contained in this map.
	 * 
	 * @return a set view of the keys contained in this map.
	 */
	public Set<K>  keySet() {
        return multimap.keySet();
	}//keySet

	/**
	 * Returns a List of objects stored under the key (key).
	 * 
	 * @return a List of objects stored under the key.
	 */
	public List<V> get(Object key) {
		return  (List)(multimap.get(key));
	}//get


    /**
     * Returns a string representation of the object
     */
    @Override
    public String toString(){
        return multimap.toString();
    }//toString
}