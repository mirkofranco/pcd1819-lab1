package lab1.statistics;

public class MultiMapTest {

    public static void main(String[] args){
        MultiMap<Integer, String> firstTest = new MultiMap<>();
        firstTest.put(1, "Prima prova prima entry");
        firstTest.put(1, "Seconda prova prima entry" );
        firstTest.put(2, "Prima prova seconda entry");
        System.out.println("--------");
        System.out.println(firstTest);
        System.out.println("--------");
        System.out.println(firstTest.keySet());
        System.out.println("--------");
        System.out.println(firstTest.get(1));
    }//main
}//MultiMapTest
