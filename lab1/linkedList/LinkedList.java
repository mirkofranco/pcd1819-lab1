package lab1.linkedList;

public class LinkedList {
 
	private int counter;
	private Node head;
 
	public LinkedList() {}

	@Override
    public String toString(){
	    String list = "";
	    Node tempNode = head;
	    while(tempNode != null){
	        list += tempNode.toString() + " ";
	        tempNode = tempNode.getNext();
        }//while
        return list;
    }//toString
 
	// appends the specified element to the end of this list.
	public void add(Object data){
	    if(head == null){
	        head = new Node(data);
	    }else{
	        Node tempNode = head;
	        while(tempNode.getNext() != null)
	            tempNode = tempNode.getNext();
	        tempNode.setNext(new Node(data));
        }//ifelse
	    counter++;
	}//add
  
	// inserts the specified element at the specified position in this list
	public void add(Object data, int index) {
 		//Se index è maggiore del numero di elementi della lista viene inserito alla fine
        if(index > counter)
            add(data);
        else{
            Node tempNode = head;
            for(int i = 1; i < index - 1 ; i++)
                tempNode = tempNode.getNext();
            Node newNode = new Node(data);
            newNode.setNext(tempNode.getNext());
            tempNode.setNext(newNode);
        }//ifelse
	}//add
 
	public Object get(int index){
	    //Se l'indice è al di fuori della lista il metodo ritorna null
 		if(index <= counter){
 		    Node tempNode = head;
 		    for(int i = 1; i < index; i++)
 		        tempNode = tempNode.getNext();
 		    return tempNode.data;
        }//if
        return null;
	}//get
 
	// removes the element at the specified position in this list.
	public boolean remove(int index) {
  		//Se l'indice è fuori dalla lista ritorna false
        if(index <= counter){
            Node tempNode = head;
            for(int i = 1; i < index - 1; i++)
                tempNode = tempNode.getNext();
            tempNode.setNext(tempNode.getNext().getNext());
            counter--;
            return true;
        }//if
        return false;
	}
 
	// returns the number of elements in this list.
	public int size() {
		return getCounter();
	}

	public int getCounter(){
        return counter;
    }
 
	private static class Node {
		// reference to the next node in the chain, or null if there isn't one.
		Node next;
 
		// data carried by this node. could be of any type you need.
		Object data;
 
		// Node constructor
		public Node(Object dataValue) {
			next = null;
			data = dataValue;
		}

		@Override
        public String toString(){
		    return data.toString();
        }//toString

		// another Node constructor if we want to specify the node to point to.
		public Node(Object dataValue, Node nextValue) {
		    data = dataValue;
		    next = nextValue;
		}//Node
 
		// these methods should be self-explanatory
		public Object getData() {
			return data;
		}
 
		@SuppressWarnings("unused")
		public void setData(Object dataValue) {
			data = dataValue;
		}
 
		public Node getNext() {
			return next;
		}
 
		public void setNext(Node nextValue) {
			next = nextValue;
		}
 
	}
	
	public static void main(String[] args) {
		 
		// Default constructor - let's put "0" into head element.
		LinkedList list = new LinkedList();
 
		// add more elements to LinkedList
		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");
 
		/*
		 * Please note that primitive values can not be added into LinkedList directly. They must be converted to their
		 * corresponding wrapper class.
		 */
 
		System.out.println("Print: list: \t\t" + list);
		System.out.println(".size(): \t\t\t\t" + list.size());
		/* Test miei
		list.add(6,6);
		System.out.println( list);
		list.add(7, 4);
		System.out.println(list);
		*/
		System.out.println(".get(3): \t\t\t\t" + list.get(3) + " (get element at index:3 - list starts from 1)");
		System.out.println(".remove(2): \t\t\t\t" + list.remove(2) + " (element removed)");
		System.out.println(".get(3): \t\t\t\t" + list.get(3) + " (get element at index:3 - list starts from 1)");
		System.out.println(".size(): \t\t\t\t" + list.size());
		System.out.println("Print again - list: \t" + list);
	}

}
